/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';
import React, {useMemo} from 'react';
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react';
import { NavigationContainer } from "@react-navigation/native";
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import AppNavigator from "./src/navigation/AppNavigator";
import {getStore, persistor} from './src/redux/Store';

const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: 'tomato',
        accent: 'yellow',
    },
};

const App: React.FC = () => {
  const store = useMemo(() => getStore(), []);

  return (
      <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
              <PaperProvider theme={theme}>
                  <NavigationContainer>
                      <AppNavigator />
                  </NavigationContainer>
              </PaperProvider>
          </PersistGate>
      </Provider>
  );
};

export default App;
