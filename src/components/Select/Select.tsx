import React, {useRef} from 'react';
import {View, Text, TouchableWithoutFeedback, TextStyle} from 'react-native';
import RNPickerSelect  from 'react-native-picker-select';
import styles from './styles';


export interface SelectItem {
    label: string,
    itemKey: string,
    value: string
}

type Props = {
    items: SelectItem[],
    value: string,
    label: string,
    placeholder: string,
    onValueChange: (item: string) => void,
    inputStyle: TextStyle,
    labelStyle: TextStyle
}

const Select:React.FC<Props> = React.memo(({items, value, label, placeholder, onValueChange, inputStyle, labelStyle}) => {
    const picker = useRef<RNPickerSelect | null>(null);

    const InputAccessoryView = () => {
        return (
            <View style={styles.modalViewMiddle}>
                <TouchableWithoutFeedback
                    onPress={() => picker.current?.togglePicker(true)}
                    hitSlop={{ top: 4, right: 4, bottom: 4, left: 4 }}>
                    <View testID="needed_for_touchable">
                        <Text style={styles.done}>Done</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    return (
        <View>
            {label && <Text style={labelStyle}>{label}</Text>}

            <View style={styles.iconContainerAndroid}>
                <Text style={styles.done}>{'>'}</Text>
            </View>

            <RNPickerSelect
                ref={picker}
                value={value}
                onValueChange={onValueChange}
                placeholder={{
                    label: placeholder,
                    value: null,
                    color: '#9EA0A4',
                }}
                // @ts-ignore
                InputAccessoryView={InputAccessoryView}
                useNativeAndroidPickerStyle={false}
                items={items}
                style={{
                    inputIOS: inputStyle,
                    inputAndroid: inputStyle,
                    ...styles
                }}
            />
        </View>
    );
});

export default Select;
