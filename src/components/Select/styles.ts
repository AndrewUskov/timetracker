import {StyleSheet} from 'react-native';
import {hv} from "../../utils";
import COLORS from '../../enums/Colors';

const {defaultStyles} = require('react-native-picker-select/src/styles');

const styles = StyleSheet.create({
    iconContainer: {
        top: hv(20),
        right: 10
    },
    iconContainerAndroid: {
        position: 'absolute',
        top: hv(35),
        right: 10
    },
    placeholder: {
        color: COLORS.GREY,
        fontSize: hv(14)
    },
    modalViewMiddle: {
        ...defaultStyles.modalViewMiddle,
        justifyContent: 'flex-end'
    },
    done: defaultStyles.done
});

export default styles;
