import {StyleSheet} from 'react-native';
import {hv, wv} from '../../utils';
import COLORS from "../../enums/Colors";


const styles = StyleSheet.create({
    container: {
        width: wv(249),
        marginTop: hv(20)
    },
    title: {
        fontSize: hv(14),
        color: COLORS.GREY
    },
    error: {
        fontSize: hv(10),
        color: COLORS.RED
    },
    input: {
        width: wv(249),
        height: hv(40),
        padding: hv(10),
        paddingHorizontal: wv(30),
        fontSize: hv(14),
        color: COLORS.BLACK
    },
    leftIcon: {
        position: "absolute",
        bottom: hv(16),
        left: wv(10),
        paddingRight: wv(10),
        borderRightWidth: 1,
        borderRightColor: COLORS.GREY
    },
    rightIcon: {
        position: "absolute",
        bottom: hv(12),
        right: wv(14),
        paddingLeft: wv(10)
    },
    rightText: {
        color: COLORS.BLACK,
        fontSize: hv(14)
    }
});

export default styles
