import React from 'react';
import {TextInput, View, Text, TextStyle} from "react-native";
import COLORS from '../../enums/Colors';
import styles from "./styles";

type Props = {
  value: string,
  label: string,
  placeholder: string,
  multiline?: boolean,
  maxLength?: number,
  numberOfLines?: number,
  onChange: (item: string) => void,
  customStyle: TextStyle,
  labelStyle: TextStyle
}

const InputText: React.FC<Props> = React.memo(props => {
  const {
    value,
    label,
    onChange,
    multiline,
    maxLength,
    placeholder,
    numberOfLines,
    customStyle,
    labelStyle
  } = props;

  return (
      <View>
        {
          label && <Text style={labelStyle}>{label}</Text>
        }

        <View style={!customStyle && [styles.container, {
          borderColor: COLORS.BORDER,
          backgroundColor: COLORS.WHITE,
          borderWidth: .5,
          borderRadius: 15,
          marginTop: 8
        }]}>
          <TextInput
              value={value}
              maxLength={maxLength}
              onChangeText={onChange}
              multiline={multiline}
              underlineColorAndroid={COLORS.TRANSPARENT}
              placeholderTextColor={COLORS.BORDER}
              placeholder={placeholder}
              style={customStyle}
              numberOfLines={numberOfLines}
          />
        </View>
      </View>
  );
});

export default InputText;
