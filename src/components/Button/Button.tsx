import React from 'react';
import {StyleProp, Text, TextStyle, TouchableOpacity, ViewStyle} from 'react-native';
import {throttle} from "lodash";

type Props = {
    name: string,
    disabled?: boolean,
    onButtonPress: () => void,
    buttonStyle: StyleProp<ViewStyle>,
    textStyle: TextStyle
}


const CustomButton: React.FC<Props> = React.memo(props => {
    const {name, textStyle, buttonStyle, onButtonPress, disabled} = props;

    return (
        <TouchableOpacity
            style={buttonStyle}
            onPress={throttle(() => onButtonPress(), 4000)}
            disabled={disabled}>
            <Text style={textStyle}>{name}</Text>
        </TouchableOpacity>
    )
});

export default CustomButton;
