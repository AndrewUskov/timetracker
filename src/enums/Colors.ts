enum COLORS {
    GREY = '#5F5F5F',
    RED = '#D65656',
    BLACK = '#000000',
    BORDER = '#D9D9D9',
    WHITE = '#ffffff',
    ORANGE = '#ffb65f',
    BLUE = '#00e3f9',
    TRANSPARENT = 'transparent'
}

export default COLORS;
