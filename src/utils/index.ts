import {Dimensions, PixelRatio, Platform} from 'react-native';

export const { width, height } = Dimensions.get('screen');

export const oldDevice = Platform.OS === 'android' || height/width < 2;

// value from designe
const designHeight = 812;
const designWidth = 375;

let widthRatio = width / designWidth;
let heightRation = height / designHeight;

export const wv = (size: number) => {
    return PixelRatio.roundToNearestPixel(size * widthRatio);
};

export const hv = (size: number) => {
    return PixelRatio.roundToNearestPixel(size * heightRation);
};


export const dlog = (message: string, ...optionalParams: any[]) => {
    if (__DEV__) {
        // eslint-disable-next-line no-console
        console.log(message, ...optionalParams)
    } else {
        // TODO: Add logging for production
    }
}
