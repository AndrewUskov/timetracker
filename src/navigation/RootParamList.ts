import {Note} from "../redux/Notes/Types";

export type RootStackParamList = {
    Notes: undefined;
    NewNote: undefined;
    Note: {
        note: Note
    };
};
