import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { RootStackParamList } from "./RootParamList";
import NotesScreen from "../screens/NotesScreen";
import NewNoteScreen from "../screens/NewNoteScreen";
import NoteScreen from "../screens/NoteScreen/NoteScreen.connector";


const Stack = createStackNavigator<RootStackParamList>();

const AppNavigator: React.FC = () => {
    return (
        <Stack.Navigator initialRouteName="Notes">
            <Stack.Screen name="Notes" component={NotesScreen} />
            <Stack.Screen name="NewNote" component={NewNoteScreen} />
            <Stack.Screen name="Note" component={NoteScreen} />
        </Stack.Navigator>
    );
};

export default AppNavigator;
