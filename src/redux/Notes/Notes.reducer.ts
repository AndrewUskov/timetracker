import { getType } from "typesafe-actions";
import {
    NoteActionTypes,
    createNote,
    deleteNote
} from "./Notes.actions";
import {Note} from "./Types";


const notesReducer = (state: Note[] = [], action: NoteActionTypes) => {
    switch (action.type) {
        case getType(createNote):
            return [...state, action.payload];
        case getType(deleteNote):
            return state.filter(item => item.id !== action.payload);
        default:
            return state;
    }
}

export default notesReducer;
