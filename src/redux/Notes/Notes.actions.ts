import { ActionType, createAction } from "typesafe-actions";
import {Note} from "./Types";


export const TYPES = {
    CREATE_NOTE: 'NOTES.CREATE',
    DELETE_NOTE: 'NOTES.DELETE'
}

export const createNote = createAction(TYPES.CREATE_NOTE)<Note>();
export const deleteNote = createAction(TYPES.DELETE_NOTE)<number>();


const notesAction = {
    createNote,
    deleteNote
}

export type NoteActionTypes = ActionType<typeof notesAction>;
