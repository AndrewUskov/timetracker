export interface Note {
    id: number,
    userId: number,
    username: string,
    tracker: string,
    website: string,
    description: string
}
