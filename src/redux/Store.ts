import { Store, applyMiddleware, createStore } from "redux";
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import reducers from "./Reducers";
import { createLogicMiddleware } from "redux-logic";
import axios from 'axios';
import logic from "./Logic";
import {dlog} from "../utils";

let store: Store;

const deps = {
    httpClient: axios
};

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: []
}

const logger = () => (next: Function) => (action: string) => {
    const state = store.getState();

    dlog(action, state);

    return next(action);
}

const persistedReducer = persistReducer(persistConfig, reducers)

const logicMiddleware = createLogicMiddleware(logic, deps);
const middleware = applyMiddleware(
    logger,
    logicMiddleware
);


const getStore = () => {
    if (store) {
        return store;
    }

    store = createStore(
        persistedReducer,
        undefined,
        middleware
    );

    return store;
};

const persistor = persistStore(getStore());

export {getStore, persistor};


