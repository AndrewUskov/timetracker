import { getType } from "typesafe-actions";
import {getUsersLoading, getUsersSuccess, getUsersError} from './Users.actions';
import { createLogic } from 'redux-logic';
import axios from "axios";

const getUsersDataLogic = createLogic({
    type: getType(getUsersLoading),  // Respond to actions of this type
    latest: true, // Only provide the latest response if fired many times

    processOptions: {
        dispatchReturn: true, // Automatically dispatch the actions below from the resolved/rejected promise

        successType: getType(getUsersSuccess), // If promise resolved, dispatch this action
        failType: getType(getUsersError) // If promise rejected, dispatch this action
    },

    // Declare our promise inside a process
    process({action}) {
        console.log('started process with action type: ' + action.type);

        return axios.get(`https://jsonplaceholder.typicode.com/users`)
            .then((resp) => resp.data);
    }
});

export default [
    getUsersDataLogic
];
