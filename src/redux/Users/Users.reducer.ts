import { getType } from "typesafe-actions";
import {
    UserActionTypes,
    getUsersLoading,
    getUsersSuccess,
    getUsersError
} from "./Users.actions";
import {User} from "./Types";


export type State = {
    users: User[];
    error: string | null;
    loading: boolean;
};

const initialState = {
    users: [],
    error: null,
    loading: false
};


const usersReducer = (state: State = initialState, action: UserActionTypes): State => {
    console.log(action);
    switch (action.type) {
        case getType(getUsersLoading):
            return {...state, loading: true, error: null, users: []};
        case getType(getUsersSuccess):
            return {...state, loading: false, error: null, users: action.payload};
        case getType(getUsersError):
            return {...state, loading: false, error: action.payload.message, users: []};
        default:
            return state
    }
};

export default usersReducer;
