import { ActionType, createAction } from "typesafe-actions";
import {User, ApiErrorResponse} from "./Types";

export const GET_USERS_LOADING = 'USERS.GET_LOADING';
export const GET_USERS_SUCCESS =  'USERS.GET_SUCCESS';
export const GET_USERS_ERROR = 'USERS.GET_ERROR';

export const getUsersLoading = createAction(GET_USERS_LOADING)();
export const getUsersSuccess = createAction(GET_USERS_SUCCESS)<User[]>();
export const getUsersError = createAction(GET_USERS_ERROR)<ApiErrorResponse>();

export const userActions = {
    getUsersLoading,
    getUsersSuccess,
    getUsersError
}


export type UserActionTypes = ActionType<typeof userActions>;
