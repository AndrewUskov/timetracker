import { combineReducers } from 'redux';
import { StateType } from 'typesafe-actions';
import users from "./Users/Users.reducer";
import notes from "./Notes/Notes.reducer";

export type RootStoreType = StateType<typeof reducers>;

const reducers = combineReducers({
    users,
    notes
});

export default reducers;
