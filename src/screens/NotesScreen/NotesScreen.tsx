import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import {useDispatch, useSelector} from "react-redux";
import {StackScreenProps} from "@react-navigation/stack";
import {RootStackParamList} from "../../navigation/RootParamList";
import {getUsersLoading} from "../../redux/Users/Users.actions";
import {deleteNote} from "../../redux/Notes/Notes.actions";
import CustomButton from "../../components/Button";
import NoteItem from './components/NoteItem';
import {RootStoreType} from "../../redux/Reducers";
import {Note} from "../../redux/Notes/Types";
import styles from './styles';



type Props = StackScreenProps<RootStackParamList, "Notes">;

const NotesScreen: React.FC<Props> = ({navigation}) => {
    const dispatch = useDispatch();
    const { notes } = useSelector((store: RootStoreType) => store);

    useEffect(() => {
        dispatch(getUsersLoading());
    }, [])

    const renderItem = ({item} : {item: Note}) =>
        <NoteItem
            note={item}
            onPress={() => navigation.navigate('Note', {note: item})}
            handleDelete={() => dispatch(deleteNote(item.id))}
        />

    return (
        <View>
            <CustomButton
                name={'Add'}
                onButtonPress={() => navigation.navigate('NewNote')}
                buttonStyle={styles.buttonStyle}
                textStyle={styles.textStyle}
            />

            <FlatList
                data={notes}
                renderItem={renderItem}
                keyExtractor={(item: Note) => item.id?.toString()}
                showsVerticalScrollIndicator={false}
                style={styles.content}
            />
        </View>
    )
};

export default NotesScreen;
