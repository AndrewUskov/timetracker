import {StyleSheet} from 'react-native';
import COLORS from "../../enums/Colors";
import {hv, wv} from "../../utils";

const styles = StyleSheet.create({
    content: {
        paddingVertical: hv(20)
    },
    buttonStyle: {
        width: wv(50),
        height: hv(20),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.ORANGE
    },
    textStyle: {
        color: COLORS.WHITE,
        fontSize: hv(12)
    }
});

export default styles;
