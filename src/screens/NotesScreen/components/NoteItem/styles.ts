import {StyleSheet} from "react-native";
import COLORS from "../../../../enums/Colors";
import {wv, hv} from "../../../../utils";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 3
    },
    buttonStyle: {
        flex: 1,
        width: wv(60),
        height: hv(30),
        backgroundColor: COLORS.RED,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        color: COLORS.WHITE,
        fontSize: hv(12)
    },
    description: {
        color: COLORS.BLUE,
        textDecorationLine: 'underline'
    }
});

export default styles;
