import React from 'react';
import {View, Text} from 'react-native';
import CustomButton from "../../../../components/Button";
import {Note} from "../../../../redux/Notes/Types";
import styles from './styles';


type Props = {
    note: Note,
    handleDelete: () => void,
    onPress: () => void
}


const NoteItem: React.FC<Props> = ({note, handleDelete, onPress}) => (
    <View style={styles.container}>
        <View style={{flex: 0.8}}>
            <Text>{note.username}</Text>
        </View>
        <View style={{flex: 1.2}}>
            <Text>{note.website}</Text>
        </View>
        <View style={{flex: 2}}>
            <Text
                onPress={onPress}
                style={styles.description}
            >{note.description}</Text>
        </View>
        <View style={{flex: 0.5}}>
            <Text>{note.tracker}</Text>
        </View>
        <CustomButton
            name={'Delete'}
            onButtonPress={handleDelete}
            buttonStyle={styles.buttonStyle}
            textStyle={styles.textStyle}
        />
    </View>
)

export default NoteItem;
