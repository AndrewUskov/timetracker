import {StyleSheet} from 'react-native';
import COLORS from "../../enums/Colors";
import {hv, wv} from "../../utils";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 20
    },
    inputStyle: {
        width: wv(308),
        height: hv(50),
        borderRadius: hv(10),
        borderWidth: 1,
        borderColor: COLORS.BORDER,
        paddingLeft: 10,
        marginBottom: 15,
        color: COLORS.BLACK
    },
    labelStyle: {
        color: COLORS.GREY,
        fontSize: hv(12),
        marginBottom: 5
    },
    buttonStyle: {
        width: wv(80),
        height: hv(40),
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.ORANGE
    },
    textStyle: {
        color: COLORS.WHITE,
        fontSize: hv(12)
    }
})

export default styles;
