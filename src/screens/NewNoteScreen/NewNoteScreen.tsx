import React, {useMemo, useState} from 'react';
import {View} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import {StackScreenProps} from "@react-navigation/stack";
import {RootStackParamList} from "../../navigation/RootParamList";
import Select from "../../components/Select";
import InputText from "../../components/InputText";
import CustomButton from "../../components/Button";
import {User} from "../../redux/Users/Types";
import {SelectItem} from "../../components/Select/Select";
import {RootStoreType} from "../../redux/Reducers";
import {Note} from "../../redux/Notes/Types";
import {createNote} from "../../redux/Notes/Notes.actions";
import styles from './styles';


type Props = StackScreenProps<RootStackParamList, "NewNote">;


const NewNoteScreen: React.FC<Props> = ({navigation}) => {
    const { users } = useSelector((store: RootStoreType) => store.users);
    const dispatch = useDispatch();

    const [note, setNote] = useState<Note>({
        id: Math.floor(Math.random() * 100000000),
        userId: 0,
        username: '',
        tracker: '',
        website: '',
        description: ''
    })

    const usersArray = useMemo(() => users.map((item: User): SelectItem => {
        return ({
            label: item.name,
            itemKey: item.name,
            value: item.name
        })
    }), [users])

    const handleChange = (key: string) => (value: string): void => {
        if(key === 'username') {
            const user = users.find((user: User) => user.name === value);
            if(user) {
                setNote({
                    ...note,
                    username: value,
                    website: user.website,
                    userId: user.id
                })
            }
        } else {
            setNote({...note, [key]: value})
        }
    }

    const handleSubmit = () => {
        dispatch(createNote(note));
        navigation.goBack()
    }

    return (
        <View style={styles.container}>
            <View>
                <Select
                    items={usersArray || []}
                    value={note?.username}
                    label={'Change user'}
                    placeholder={'Change user'}
                    onValueChange={handleChange('username')}
                    inputStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                />

                <InputText
                    value={note.tracker}
                    label={'Enter a track time'}
                    placeholder={'Enter a track time'}
                    onChange={handleChange('tracker')}
                    customStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                />

                <InputText
                    value={note.description}
                    label={'Enter description'}
                    placeholder={'Enter description'}
                    onChange={handleChange('description')}
                    customStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                />
            </View>


            <CustomButton
                name={'Submit'}
                onButtonPress={handleSubmit}
                buttonStyle={styles.buttonStyle}
                textStyle={styles.textStyle}
            />
        </View>
    )
};

export default NewNoteScreen;
