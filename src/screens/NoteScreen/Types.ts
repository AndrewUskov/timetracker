import {StackScreenProps} from "@react-navigation/stack";
import {RootStackParamList} from "../../navigation/RootParamList";
import {State} from "../../redux/Users/Users.reducer";

export type Props = StackScreenProps<RootStackParamList, "Note"> & State;
