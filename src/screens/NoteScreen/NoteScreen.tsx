import React, {useMemo} from 'react';
import {View, Text} from 'react-native';
import {withTheme, Card} from 'react-native-paper';
import {Props} from "./Types";
import styles from "./styles";


const NoteScreen: React.FC<Props> = ({route, users}) => {
    const {note} = route.params;

    const noteCreatorsInfo = useMemo(() => users?.find(user => user.id === note.userId),
        [note.userId]);

    const phone = useMemo(() => noteCreatorsInfo?.phone.split(' ')[0],
        [noteCreatorsInfo]);

    return (
        <View style={styles.container}>
            <Card style={{ margin: 10, padding: 10 }}>
                <View style={{ flex: 0 }}>
                    <Text style={styles.description}>{note.description}</Text>

                    <View style={styles.contentWrapper}>
                        <Text style={styles.contentHeader}>Note creator:</Text>
                        <Text>Name: {noteCreatorsInfo?.name}</Text>
                        <Text>City: {noteCreatorsInfo?.address.city}, {noteCreatorsInfo?.address.street}</Text>
                        <Text>ZIP: {noteCreatorsInfo?.address.zipcode}</Text>
                    </View>

                    <View style={styles.contentWrapper}>
                        <Text>Tel: {phone}</Text>
                        <Text>Website: {note.website}</Text>
                    </View>
                </View>
            </Card>

        </View>
    )
};

export default withTheme<{
    theme: ReactNativePaper.Theme;
}, any>(NoteScreen);
