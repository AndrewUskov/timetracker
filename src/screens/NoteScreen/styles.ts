import {StyleSheet} from "react-native";
import COLORS from "../../enums/Colors";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 10
    },
    description: {
        alignSelf: 'center',
        marginVertical: 20,
        fontSize: 18,
        fontWeight: 'bold'
    },
    contentWrapper: {
        marginBottom: 10
    },
    contentHeader: {
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 10,
        color: COLORS.ORANGE
    }
});

export default styles;
