import {connect} from "react-redux";
import {RootStoreType} from "../../redux/Reducers";
import {State} from "../../redux/Users/Users.reducer";
import {UserActionTypes} from "../../redux/Users/Users.actions";
import NoteScreen from "./NoteScreen";

export function mapStateToProps(state: RootStoreType): State {
    return {
        users: state.users.users,
        error: state.users.error,
        loading: state.users.loading
    };
}

export default connect<
    State,
    UserActionTypes,
    {},
    RootStoreType
>(mapStateToProps)(NoteScreen);
